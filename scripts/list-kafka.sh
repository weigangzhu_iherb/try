#!/bin/bash
NAMESPACE=catalog
CONTEXTS=(
    # "test"
    preprod
    # "frankfurt"
    # "hongkong"
    # "seoul"
    # "sydney"
    # "tokyo"
    # "virginia"
    # "netherlands"
    # "bahrain"
    # "london"
    # "oregon"
    # "singapore"      
    # "taiwan"
)

for CONTEXT in "${CONTEXTS[@]}"; do
    echo -e "\n\nSTART $CONTEXT $NAMESPACE\n" 
    # POD="kafka-0"
    POD=$(kubectl --context $CONTEXT -n $NAMESPACE get pod -l app=prometheus  -o jsonpath="{.items[0].metadata.name}")
    echo "POD: $POD"

    kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -c prometheus -- df -h | grep prometheus
    # kubectl --context $CONTEXT -n $NAMESPACE rollout restart statefulset service-gateway
    # kubectl --context $CONTEXT -n $NAMESPACE get pods | grep service-gateway
    # kubectl --context $CONTEXT -n $NAMESPACE scale deployment.v1.apps/consumer-gateway --replicas=10
    # kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- kafka-consumer-groups --describe --group consumer-gateway --bootstrap-server localhost:9092 --export | grep Desktop-Product  # | awk '{sum+=$5} END {print sum}'
    # kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- kafka-consumer-groups --group consumer-gateway --bootstrap-server localhost:9092 --topic UpsertPageRequestCacheEntity-Mobile-Product --reset-offsets --to-latest --execute
    # kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- kafka-consumer-groups --group consumer-gateway --bootstrap-server localhost:9092 --topic UpsertPageRequestCacheEntity-Desktop-Product --reset-offsets --to-latest --execute
    # kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- kafka-topics --zookeeper kafka-zookeeper-headless:2181 --describe | grep UpsertPageRequestCacheEntity
    echo -e "\n\nEND $CONTEXT $NAMESPACE\n\n"
done
