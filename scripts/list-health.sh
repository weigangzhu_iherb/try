#!/bin/bash

# http://localhost:9200/page-content/_search?q=request.pageType:Home%20AND%20request.country:IN AND request.language:hi-IN
NAMESPACE=catalog
CONTEXTS=(
    # "bahrain"
    # "frankfurt"    
    "london"
    "netherlands"
    # "oregon"
    "seoul"
    "hongkong"
    "sydney"    
    "tokyo"
    "virginia"
    "singapore"      
)

for CONTEXT in "${CONTEXTS[@]}"; do
    echo -e "\n\nSTART $CONTEXT $NAMESPACE\n" 
    kubectl --context $CONTEXT -n $NAMESPACE rollout restart deployment service-category
    # POD=$(kubectl --context $CONTEXT -n $NAMESPACE get pod -l app=api-catalog -o jsonpath="{.items[0].metadata.name}")
    # echo "POD: $POD"
    
  #   kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- curl http://127.0.0.1/info/freeshippingmin \
  # -H 'pragma: no-cache' \
  # -H 'cache-control: no-cache' \
  # -H 'upgrade-insecure-requests: 1' \
  # -H 'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1' \
  # -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  # -H 'sec-fetch-site: none' \
  # -H 'sec-fetch-mode: navigate' \
  # -H 'sec-fetch-user: ?1' \
  # -H 'sec-fetch-dest: document' \
  # -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' \
  # -H 'cookie: dscid=a6fd5d86-a2a8-423f-bd4f-bb7491520241; _gcl_au=1.1.539523972.1604971622; ih-exp-recs=1; __auc=7a765b97175afc36f3a4996087c; _scid=22d9beb7-6462-422b-8eb7-1cfb5926dfe1; _fbp=fb.1.1604971623160.1088497691; _ts_yjad=1605591217614; crl8.fpcuid=ffdb45b5-1ce5-486f-83e5-b3a9d06e2719; ih-prospect-mailing-banner-hidden=true; _gcl_aw=GCL.1607321716.EAIaIQobChMIsvu6ope77QIVJNxMAh2KJAVhEAQYAyABEgLPmfD_BwE; _gcl_dc=GCL.1607321716.EAIaIQobChMIsvu6ope77QIVJNxMAh2KJAVhEAQYAyABEgLPmfD_BwE; _gac_UA-229961-54=1.1607321716.EAIaIQobChMIsvu6ope77QIVJNxMAh2KJAVhEAQYAyABEgLPmfD_BwE; tmr_lvid=389552a2536bb18ef9062d9c6c76dfca; tmr_lvidTS=1607573673413; _ym_uid=1607573674334079444; _ym_d=1607573674; tmr_reqNum=101; ihr-search-0=%5B%22tea%22%5D; Hm_lvt_bb2490e630a6b5f60d57bbe3a72ed263=1607588132,1608035728,1608276986; Qs_lvt_82139=1607588130%2C1608035721%2C1608088381%2C1608273589%2C1608694276; Qs_pv_82139=4578160594652116500%2C2992438582145282600%2C547960102838764160%2C86916879466523420%2C1221036796987310800; ih-hp-vp=35; ih-hp-view=1; ih-experiment=eyJzZWFyY2giOnsiQ2hvc2VuVmFyaWFudCI6MCwiRW5kRGF0ZSI6IjIwMjEtMDQtMDhUMDA6MDA6MDAifSwic2VhcmNoQ2F0ZWdvcnlCb29zdCI6eyJDaG9zZW5WYXJpYW50IjowLCJFbmREYXRlIjoiMjAyMS0wNC0wOFQwMDowMDowMCJ9fQ==; __cfduid=d25698ec2809fbe2da594c4e003dee41d1610431328; ihr-session-id1=aid=b47d1e26-733a-4e83-a1b3-cb878209ce71; ihr-ocid1=b47d1e26-733a-4e83-a1b3-cb878209ce71; _pin_unauth=dWlkPU56WTFaR1EyWlRjdFl6Rm1NUzAwTmpnd0xXRTVNbVl0TWprME0ySmtOVFZpTW1RMg; ForceTrafficSplitType=B; _gid=GA1.2.476780952.1611734018; pref-saved=2; _sctr=1|1611734400000; ihr-ds-vps=23160,62118,102928,7765,48102,96239,58127,100582,63681,14132; iher-vps=23160.62118.7765.48102.96239.58127.100582.63681.14132.61864.33119.98308.21133.2796.22335.33120.59561.64902.84969.94124.2782.53978.23576.69066.16670.71106.45934.54850.101714.18419; srid=; ihr-temse=expires=27%20Jan%202021%2011:40:02Z&cc=US; __asc=1b0908ea177436dc76ef893e8e7; _dc_gtm_UA-229961-54=1; fs_uid=rs.fullstory.com#RG1FH#6198255951396864:6195800043274240/1642735123; iher-pref1=accsave=0&bi=0&ifv=1&lan=ja-JP&lchg=1&sccode=JP&scurcode=JPY&storeid=0&wp=1; ih-preference=store=0&country=JP&language=ja-JP&currency=JPY&weight=1; _uetsid=c3705710607411eb802317c867c872a4; _uetvid=d60fcb2022f311ebbbd66bd9af174b47; _ga_P031MWVETB=GS1.1.1611744001.61.1.1611744014.0; _ga=GA1.1.1574532682.1604971622; _ga_SW3NJP516F=GS1.1.1611744001.61.1.1611744014.47; __CG=u%3A4105609585205336000%2Cs%3A1086908433%2Ct%3A1611744015812%2Cc%3A2%2Ck%3Ahk.iherb.com%2F21%2F21%2F80%2Cf%3A1%2Ci%3A1' \
  # --compressed
    echo -e "\n\nEND $CONTEXT $NAMESPACE\n\n"
done
