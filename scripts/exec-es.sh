#!/bin/bash
NAMESPACE=catalog
CONTEXTS=(
    "bahrain"
    "frankfurt"
    "hongkong"
    "london"
    "netherlands"
    "oregon"
    "seoul"
    "sydney"
    "taiwan"
    "tokyo"
    "virginia"
    "singapore"      
)

for CONTEXT in "${CONTEXTS[@]}"; do
    echo -e "\n\nSTART $CONTEXT $NAMESPACE\n" 
    POD="elasticsearch-master-0"
    echo "POD: $POD"


    # kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- curl --location --request POST 'http://localhost:9200/page-content/_search' \
    #     --header 'Content-Type: application/json' \
    #     -d '{
    #     "query": {
    #             "query_string": {
    #             "query": "request.country:(UA OR AO OR MG OR MZ OR CI OR SN OR MG OR UA OR AO OR CI OR MZ OR SN) AND request.language:en-US"
    #             }
    #         },
    #         "_source":"id"
    #     }'

    kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- curl --location --request POST 'http://localhost:9200/page-content/_count'

    # kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- curl --location --request POST 'http://localhost:9200/page-content/_count' \
    #     --header 'Content-Type: application/json' \
    #     -d '{
    #     "query": {
    #             "query_string": {
    #             "query": "request.pageType:(Home) AND request.platform:Desktop"
    #             }
    #         }
    #     }'
    
    # kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- curl --location --request POST 'http://localhost:9200/page-content/_delete_by_query' \
    #     --header 'Content-Type: application/json' \
    #     -d '{
    #     "query": {
    #             "query_string": {
    #             "query": "request.country:(UA OR AO OR MG OR MZ OR CI OR SN OR MG OR UA OR AO OR CI OR MZ OR SN) AND request.language:en-US"
    #             }
    #         }
    #     }'

    echo -e "\n\nEND $CONTEXT $NAMESPACE\n\n"
done
