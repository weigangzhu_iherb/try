#!/bin/bash
NAMESPACE=catalog
CONTEXTS=(
    "bahrain"
    "frankfurt"
    "hongkong"
    "london"
    "netherlands"
    "oregon"
    "seoul"
    "sydney"
    "taiwan"
    "tokyo"
    "virginia"
    "singapore"      
)

for CONTEXT in "${CONTEXTS[@]}"; do
    echo -e "\n\nSTART $CONTEXT $NAMESPACE\n" 
    POD="$1"
    echo "POD: $POD"
    
    kubectl --context $CONTEXT -n $NAMESPACE get pods | grep $POD
    echo -e "\n\nEND $CONTEXT $NAMESPACE\n\n"
done
