#!/bin/bash
NAMESPACE=catalog
CONTEXTS=(
    # "preprod"
    "bahrain"
    "frankfurt"
    "hongkong"
    "london"
    # "netherlands"
    "oregon"
    "seoul"
    "sydney"
    # "taiwan"
    "tokyo"
    "virginia"
    "singapore"      
)

if [ -z "$1" ]; then
    echo "please input app name";
    exit;
fi

if [ -z "$2" ]; then
    echo "please input shell command"
    exit;
fi

for CONTEXT in "${CONTEXTS[@]}"; do
    echo -e "\n\nSTART $CONTEXT $NAMESPACE\n" 
    POD=$(kubectl --context $CONTEXT -n $NAMESPACE get pod -l app=$1 -o jsonpath="{.items[0].metadata.name}")
    echo "POD: $POD"
    
    kubectl --context $CONTEXT -n $NAMESPACE exec -ti $POD -- sh -c "$2"
    echo -e "\n\nEND $CONTEXT $NAMESPACE\n\n"
done

#4546f02a2b23d2f3614ceaf950592449
#ca1d6dd18ec979bdad3c71aa3766444b

#bash .\scripts\exec-pods.sh service-gateway "curl -X DELETE http://elasticsearch-master:9200/page-content/_doc/4546f02a2b23d2f3614ceaf950592449"
#bash .\scripts\exec-pods.sh service-gateway "curl -X DELETE http://elasticsearch-master:9200/page-content/_doc/ca1d6dd18ec979bdad3c71aa3766444b"