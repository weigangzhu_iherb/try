#!/bin/bash
NAMESPACE=catalog
CONTEXTS=(
    "bahrain"
    "frankfurt"
    "hongkong"
    "london"
    # "netherlands"
    "oregon"
    "seoul"
    "sydney"
    # "taiwan"
    "tokyo"
    "virginia"
    "singapore"      
)

for CONTEXT in "${CONTEXTS[@]}"; do
    echo -e "\n\nSTART $CONTEXT $NAMESPACE\n" 
    
    kubectl --context $CONTEXT -n $NAMESPACE get hpa graphql-gateway
    echo -e "\n\nEND $CONTEXT $NAMESPACE\n\n"
done
